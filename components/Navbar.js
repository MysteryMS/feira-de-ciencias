import { Flex, Link, Text } from "@chakra-ui/react";

const Navbar = () => {
    return (
        <Flex width={"100%"} p={"20px 60px 20px 20px"} justifyContent={"space-between"} alignItems={"center"} mb={"3rem"} backgroundColor={"white"}>
            <Flex>
                <Text fontSize={"2xl"}>🧪 Feira de Experiência</Text>
            </Flex>
            <Flex>
                <Link>Home</Link>
            </Flex>
        </Flex>
    )
}

export default Navbar;
