import { Flex, Image, Box, Text, Avatar, AvatarGroup, Link } from "@chakra-ui/react";
import NextLink from "next/link";

const VideoCard = ({ video }) => {
    return (
        <NextLink href={"/video/1"} passHref>
            <Link>
                <Flex width={"20rem"} direction={"column"} borderRadius={"20px"} border={"1px solid black"}>
                    <Box>
                        <Image src={"/marina.jpg"} borderRadius={"20px 20px 0 0"} mb={"1rem"} />
                    </Box>
                    <Box>
                        <Text px={"10px"} as={"b"}>Marina coloca fogo na sua casa!</Text>
                    </Box>
                    <Box p={"10px"}>
                        <AvatarGroup size={"sm"}>
                            <Avatar name="Marina" />
                            <Avatar name="Marina" />
                            <Avatar name="Marina" />
                            <Avatar name="Marina" />
                        </AvatarGroup>
                    </Box>
                </Flex>
            </Link>
        </NextLink>
    )
}

export default VideoCard;
