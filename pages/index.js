import { Container, extendTheme } from '@chakra-ui/react'
import Head from 'next/head'
import Image from 'next/image'
import Navbar from '../components/Navbar'
import VideoCard from '../components/VideoCard'
import styles from '../styles/Home.module.css'

const theme = extendTheme({
  
})


export default function Home() {
  return (
    <div>
      <Navbar />
      <Container maxW={"100%"}>
        <VideoCard />
      </Container>
    </div>
  )
}
