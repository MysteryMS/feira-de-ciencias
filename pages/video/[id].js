import { Avatar, AvatarGroup, Box, Container, Flex, Text } from "@chakra-ui/react"
import Navbar from "../../components/Navbar"

export default function Video() {
    return (
        <div>
            <Navbar />
            <Container maxW={"100%"}>
                <Flex sx={{
                        '@media screen and (max-width: 768px)': {
                            flexWrap: "wrap"
                        }
                    }}>
                    <Flex width={"100rem"} justifyContent={"center"} direction={"column"}>
                        <Box backgroundColor={"black"}>
                            <video src="https://cdn.discordapp.com/attachments/696797678356398671/1020815245159186522/Video_4_CinWGJJvrt0.mp4" controls width={"100%"} style={{ maxHeight: "30rem" }} />
                        </Box>
                        <Text as={"b"} fontSize={"3xl"}>TITLE</Text>
                        <Box>
                            <AvatarGroup>
                                <Avatar name="Marina" />
                                <Avatar name="Marina" />
                                <Avatar name="Marina" />
                                <Avatar name="Marina" />
                            </AvatarGroup>
                        </Box>
                    </Flex>
                    <Flex w={"100%"} justifyContent={"center"}>
                        <Text fontSize={"3xl"}>Relacionado</Text>
                    </Flex>
                </Flex>
            </Container>
        </div>
    )
}
